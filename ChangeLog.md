### Change Log (2014.11.16) ###
* Changed the Quadcopter aircraft type to Multirotor
* Added another damage assessment type named Minor
* Changed damage assessment colors in model use grid
    * Green = no damage
    * Yellow = minor damage
    * Orange = moderate damage
    * Red = severe damage
* Fixed an issue when adding the first battery or model where the label is equal to 2 instead of 1.
* Fixed an issue where the charge cycles grid was refreshing twice
* Fixed an issue where the model use grid was refreshing twice
* Updated the database version from version 1 to version 2
