# RC Sidekick #
*****
[ChangeLog](https://bitbucket.org/rcsidekick/rcsidekick/src/93ab447138de3d8820a6e084e9f228adb898e185/ChangeLog.md?at=master)
*****
### Downloads ###
[rcsidekick-1.6.0.2 (zip file)](https://bitbucket.org/rcsidekick/rcsidekick/downloads/RCSidekick-1.6.0.2.zip)

[rcsidekick-1.6.0.2 (7zip file)](https://bitbucket.org/rcsidekick/rcsidekick/downloads/RCSidekick-1.6.0.2.7z)
*****
### Installation Instructions ###
1. Unzip RCSidekick-1.6.0.2
2, If you do not have a previous version of RC Sidekick, skip to step 4
3. Copy rcsidekick.db from your previous version of RC Sidekick and paste it into the RCSidekick-1.6.0.2 folder
4. In the RCSidekick-1.6.0.2 folder, double-click on RCSidekick.exe